const _ = require('lodash');
const hotels = require('../../data/hotels');
const _f = require('../../helpers/filter');

exports.getHotels = (req, res) => {

    let response = [];

    let { name, stars } = req.query;
    let filterKeys = { name };
    let filterStars = [{ checked: true, value: 0, name: 'Todas las Estrellas' }, { checked: false, value: 5, name: '5 Estrellas' }];

    try {
        response = hotels.filter((hotel) => filterHotels(hotel, filterKeys, filterStars));
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ 'codeError': 500, 'error': error });
    }

    function filterHotels(hotel, filterKeys, filterStars) {
        if ( (isAllStars(filterStars) || isEmpty(filterKeys['name'])) ) {
            return true;
        }

        /* if( hotel ){

        } */

        return false;
    }

    function isAllStars(filterStars) {
        let out = filterStars.filter(filter => filter.value == 0 && filter.checked);
        return out.length > 0;
    }

    function isEmpty(key) {
        if (key == undefined || key == "") {
            return true;
        }
        return false;
    }

    /*         console.log(stars)
            console.log(filterStars)
            
            //Key filters
            response = hotels.filter((item) => {
                let isfiltered = false;
                for (var key in filterKeys) {
                    
                    if (filterKeys[key] != undefined) {
                        let hotelKeyValue = item[key] ? item[key].toString().toLowerCase() : item[key];
                        if (_.includes(hotelKeyValue, filterKeys[key].toLowerCase()) && _f.filterStars(item, filterStars))
                            isfiltered = true;
                    } 
                }
                return isfiltered;
            }); */


}